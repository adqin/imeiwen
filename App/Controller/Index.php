<?php

/**
 * 首页.
 * 
 * @author aic <41262633@qq.com>
 */

namespace Controller;

/**
 * Class Controller Index.
 */
class Index extends \Controller\Base {

    /**
     * 推荐文章.
     * 
     * @return void
     */
    public function index() {
        // 推荐文章.
        $page = $this->getGet('page');
        $page = $page ? $page : 1;
        $total_page = file_get_contents(CACHE_PATH . 'recommend/cache.page.num');
        $page = (!$page || $page > $total_page) ? 1 : $page;

        $result = file_get_contents(CACHE_PATH . 'recommend/cache.recommend.' . $page);
        $result = $result ? json_decode($result, true) : [];
        $list = $result ? $result : [];
        $this->assign('page', $page);
        $this->assign('total_page', $total_page);
        $this->assign('post_list', $list);
        $this->assign('this_menu', 'index');
        $this->display('home/index');
    }

    /**
     * 文章浏览次数.
     * 
     * @return void
     */
    public function postview() {
        $post_id = $this->getGet('post_id');
        if (!$post_id || strlen($post_id) != 8) {
            echo '';
            exit;
        }

        if (!\Db::instance()->exists("select `id` from `post` where `post_id` = '$post_id'")) {
            echo '';
            exit;
        }

        // 更新post.update_time
        $param = [
            'update_time' => time(),
        ];
        \Db::instance()->updateByWhere('post', $param, "post_id='$post_id'");

        $row = \Db::instance()->getRow("select `id`,`views` from `post_view` where `post_id` = '$post_id'");
        if (!$row) {
            $param = [
                'post_id' => $post_id,
                'views' => 1,
                'latest_time' => time(),
            ];
            \Db::instance()->insert('post_view', $param);
            echo '1';
            exit;
        } else {
            $id = $row['id'];
            $views = $row['views'] + 1;
            $param = [
                'views' => $views,
                'latest_time' => time(),
            ];
            \Db::instance()->updateById('post_view', $param, $id);
            echo $views;
            exit;
        }
    }

    /**
     * 时光流转
     */
    public function time() {
        $r = new \Logic\Time();
        $list = $r->GetList();
        $this->assign('list', $list);
        $this->display('/home/time');
    }

    /**
     * 微信认证
     * 
     * @return void
     */
    public function wxsign() {
        //微信JSDK验证.
        $appid = 'wx5123033be8c14a8a';
        $secret = '26000e76b7db9e90598d0cff21616b32';
        $url = $this->getPost('thisurl'); //获取当前页面的url，接收请求参数
        $root['url'] = $url;
        //获取access_token，并缓存
        $file = CACHE_PATH . 'cache.access_token'; //缓存文件名access_token
        $expires = 3600; //缓存时间1个小时
        if (file_exists($file)) {
            $time = filemtime($file);
            if (time() - $time > $expires) {
                $token = null;
            } else {
                $token = file_get_contents($file);
            }
        } else {
            fopen("$file", "w+");
            $token = null;
        }

        if (!$token || strlen($token) < 6) {
            $res = file_get_contents("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$appid}&secret={$secret}"); //自己的appid,通过微信公众平台查看appid和AppSecret
            $res = json_decode($res, true);
            $token = $res['access_token'];
            @file_put_contents($file, $token);
        }

        //获取jsapi_ticket，并缓存
        $file1 = CACHE_PATH . 'cache.jsapi_ticket';
        if (file_exists($file1)) {
            $time = filemtime($file1);
            if (time() - $time > $expires) {
                $jsapi_ticket = null;
            } else {
                $jsapi_ticket = file_get_contents($file1);
            }
        } else {
            fopen("$file1", "w+");
            $jsapi_ticket = null;
        }

        if (!$jsapi_ticket || strlen($jsapi_ticket) < 6) {
            $ur = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=$token&type=jsapi";
            $res = file_get_contents($ur);
            $res = json_decode($res, true);
            $jsapi_ticket = $res['ticket'];
            @file_put_contents($file1, $jsapi_ticket);
        }

        $timestamp = time(); //生成签名的时间戳
        $metas = range(0, 9);
        $metas = array_merge($metas, range('A', 'Z'));
        $metas = array_merge($metas, range('a', 'z'));
        $nonceStr = '';
        for ($i = 0; $i < 16; $i++) {
            $nonceStr .= $metas[rand(0, count($metas) - 1)]; //生成签名的随机串
        }

        $string1 = "jsapi_ticket=" . $jsapi_ticket . "&noncestr=$nonceStr" . "&timestamp=$timestamp" . "&url=$url";
        $signature = sha1($string1);
        $root['appid'] = $appid;
        $root['nonceStr'] = $nonceStr;
        $root['timestamp'] = $timestamp;
        $root['signature'] = $signature;

        echo json_encode($root);
        exit;
    }

}
