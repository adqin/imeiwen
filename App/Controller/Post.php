<?php

/**
 * 文章详情页.
 * 
 * @author aic <41262633@qq.com>
 */

namespace Controller;

/**
 * Class Post.
 */
class Post extends \Controller\Base {

    /**
     * 文章详情页.
     * 
     * @return void
     */
    public function item() {
        $post_id = isset($this->param['post_id']) && $this->param['post_id'] ? $this->param['post_id'] : '';        
        $itemer = new \Logic\PostItemer($post_id);
        $info = $itemer->get();
        $share = $this->getGet('share');
        $share = $share ? $share : ''; // qq或微信分享.
        
        $post_image_url = \Config\Qiniu::$domain . $info['image_url'] . '?' . $info['image_uptime'];
        $share_image_url = urlencode($post_image_url);
        
        $this->assign('info', $info);
        $this->assign('share', $share);
        $this->assign('post_image_url', $post_image_url);
        $this->display('home/post');
    }

}
