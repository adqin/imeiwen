<?php

/**
 * 系统配置.
 * 
 * @author aic <41262633@qq.com>
 */

namespace Config;

/**
 * Class Config.
 */
class System {

    /**
     * 网站信息.
     * 
     * @var array.
     */
    public static $site = array(
        'name' => '爱美文',
        'domain' => 'imeiwen.org',
        'title' => '爱美文',
        'keywords' => '爱美文, 短篇美文, 诗歌, 短诗, 古诗文',
        'note' => '爱美文，精典美文诗歌收集珍藏',
    );

    /**
     * 文章分类.
     * 
     * @var array. 
     */
    public static $category = array(
        '1' => '美文',
        '2' => '短篇美文',
        '3' => '诗歌',
        '4' => '短诗',
        '5' => '古诗文',
    );

}
