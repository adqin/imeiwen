<?php

namespace Config;

class Route {

    /**
     * 路由规则.
     * 
     * @var array. 
     */
    public static $routeRules = array(
        '/' => ['handler' => 'Index@index'], // 首页
        '/post/{post_id:\w+}' => ['handler' => 'Post@item'], // 文章详情页
        '/page/{id:\w+}' => ['handler' => 'Page@index'], // 单页
        '/time' => ['handler' => 'Index@time'], // 时间之线
    );

}
