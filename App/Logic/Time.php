<?php

/**
 * 时光流转逻辑封装.
 */

namespace Logic;

class Time {

    private $list = [];
    private $min_year = 2015;

    public function __construct() {
        
    }

    public function GetList() {
        $now = new \DateTime("now");
        while (true) {
            try {
                $year = $now->format("Y");
                if ($year < $this->min_year) {
                    /**
                     * 小于最低年限,直接退出
                     */
                    break;
                }
                $start_time = strtotime($year . '-01-01 00:00:00');
                $end_time = strtotime(($year + 1) . '-01-01 00:00:00');
                $data = $this->GetData($start_time, $end_time);
                $this->list[$year] = $data;

                /**
                 * 减一年时间,继续查询
                 */
                $now->modify("-1 year");
            } catch (\Exception $e) {
                break;
            }
        }

        return $this->list;
    }

    /**
     * 查询获取指定时间的数据.
     */
    private function GetData($start_time, $end_time) {
        $sql = "select p.post_id,p.title,p.author,p.input_time,view.views from post as p,post_view as view where p.post_id=view.post_id and p.input_time >= $start_time and p.input_time < $end_time and p.status=1 order by p.input_time desc";
        $rs = \Db::instance()->getList($sql);
        return $rs;
    }

}
