<?php

/**
 * 前台页面逻辑封装.
 * 
 * @author aic <41262633@qq.com>
 */

namespace Logic;

/**
 * Class Homer.
 */
class Homer {

    /**
     * 获取页面内容.
     * 
     * @param string $page_id 页面page id.
     * @param boolean $force_reload 强制刷新缓存.
     * 
     * @return array.
     */
    public static function getCachePage($page_id = '', $force_reload = false) {
        $cache_dir = CACHE_PATH . 'page/';
        $cache_file = $cache_dir . 'cache.' . $page_id;
        $from_db = false;
        $return = [];

        if ($force_reload) {
            // 强制从数据库中获取.
            $from_db = true;
        } elseif (!file_exists($cache_file)) {
            // 如果文件不存在.
            $from_db = true;
        } else {
            // 文件存在.
            $result = file_get_contents($cache_file);
            $return = $result ? json_decode($result, true) : [];
            if (!$return) {
                $from_db = true;
            }
        }

        if ($from_db) {
            // 需要从数据库中获取.
            $return = \Db::instance()->getRow("select * from `single_page` where `identify` = '$page_id'");
            // 下面更新缓存.
            if (!file_exists($cache_dir)) {
                mkdir($cache_dir, 0777, true);
            }
            file_put_contents($cache_file, json_encode($return));
        }

        if (!$return) {
            \Common::showErrorMsg("页面: {$page_id}, 内容为空");
        }

        return $return;
    }

}
