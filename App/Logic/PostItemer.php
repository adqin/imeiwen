<?php

/**
 * 文章详情.
 */

namespace Logic;

class PostItemer {

    private $post_id = ''; // 文章的post_id.
    private $cache_dir = ''; // 缓存目录.
    private $cache_file = ''; // 对应的缓存文件名.
    private $info = array(); // 文章信息.
    private $category = 0; // 分类信息
    private $force_reload = false; // 是否强制更新缓存.

    public function __construct($post_id = '', $force_reload = false) {
        $this->post_id = $post_id;

        // 判断参数.
        if (!$post_id) {
            \Common::showErrorMsg('post_id参数为空');
        }

        // 判断post_id是否存在.
        $row = \Db::instance()->getRow("select `id`,`category` from `post` where `post_id`='$post_id'");
        if (!$row) {
            \Common::showErrorMsg("post_id: {$post_id}, 文章不存在");
        }

        $this->category = $row['category'];
        $this->force_reload = $force_reload;

        // post缓存目录.
        $this->cache_dir = CACHE_PATH . 'post/' . $this->post_id[0] . '/' . $this->post_id[1] . '/';
        if (!file_exists($this->cache_dir)) {
            mkdir($this->cache_dir, 0777, true);
        }

        $this->cache_file = $this->cache_dir . 'cache.' . $this->post_id;
    }

    /**
     * 获取内容.
     * 
     * @return array.
     */
    public function get() {
        if ($this->force_reload) {
            // 如果强制更新缓存.
            $this->set();
        } elseif (!file_exists($this->cache_file)) {
            // 如果缓存文件不存在.
            $this->set();
        } else {
            // 从缓存中取文件.
            $cache_info = file_get_contents($this->cache_file);
            $info = $cache_info ? json_decode($cache_info, true) : [];
            if (!$info) {
                $this->set();
            } else {
                $this->info = $info;
            }
        }

        return $this->info;
    }

    /**
     * 设置缓存.
     * 
     * @return void
     */
    private function set() {
        // 查询post信息.
        $info = \Db::instance()->getRow("select * from `post` where `post_id` = '$this->post_id'");
        // 查询浏览信息.
        $view = \Db::instance()->getRow("select * from `post_view` where `post_id` = '$this->post_id'");

        // 查询关联文章.
        $relation = $this->getRelation();

        $return = [
            'post_id' => $info['post_id'],
            'title' => $info['title'],
            'author' => $info['author'],
            'category' => $info['category'],
            'image_url' => $info['image_url'],
            'image_uptime' => $info['image_uptime'],
            'content' => $info['content'],
            'long_title' => $this->cleanString($info['long_title']),
            'description' => $this->cleanString($info['description']),
            'input_time' => $info['input_time'],
            'update_time' => $info['update_time'],
            'page_view' => $view ? $view['views'] : 0,
            'relation' => $relation,
        ];

        $this->info = $return;

        // 更新文件缓存.
        file_put_contents($this->cache_file, json_encode($return));
    }

    /**
     * 更多文章
     * 
     * @return array.
     */
    private function getRelation() {
        $where = "`status` ='1' and `post_id` <> '$this->post_id'";
        return \Db::instance()->getList("select `post_id`,`title`,`author` from `post` where $where order by rand() limit 10");
    }

    /**
     * 对数据进行清理.
     * 
     * @param string 清理入参.
     * 
     * @return string.
     */
    private function cleanString($string = '') {
        $string = strip_tags($string);
        $string = str_replace("\n", "", $string);
        $string = trim($string);
        return $string;
    }

    /**
     * 析构函数.
     */
    public function __destruct() {
        unset($this->info);
    }

}
