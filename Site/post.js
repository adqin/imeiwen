layui.use(['jquery', 'element', 'util', 'layer'], function () {
    var $ = layui.$, element = layui.element, util = layui.util, layer = layui.layer;
    itemSet();

    // 浏览器窗口大小发生变化.
    $(window).resize(function () {
        itemSet();
    });

    // 刷新浏览次数.
    var post_id = $('#post_id').html();
    $.get('/index/postview', {post_id: post_id}, function (re) {
        if (re) {
            $('#page_view').html(re).show();
        }
    });

    // 回到顶部.
    util.fixbar({
        bar1: false,
        bar2: false,
        bgcolor: '#2F4056',
        click: function () {
        }
    });

    function itemSet() {
        var w = $(window).width() - 4;
        w = w > 760 ? 760 : w;
        var post_img_url = $('#post-image-id').attr('src');
        var img = new Image();
        img.src = post_img_url;
        img.onload = function () {
            $('#post-image-id').attr('src', post_img_url).width(w);
        }
    }
});