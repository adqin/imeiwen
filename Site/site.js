layui.use(['jquery', 'element', 'form', 'util'], function () {
    var $ = layui.$, element = layui.element, form = layui.form, util = layui.util;
    windowResize();
    lazyRender();

    // 浏览器窗口大小发生变化
    $(window).resize(function () {
        windowResize();
    });

    // 懒加载
    $(window).on('scroll', function () {
        lazyRender();
    });

    // 回到顶部.
    util.fixbar({
        bar1: false,
        bar2: false,
        bgcolor: '#2F4056',
        click: function () {
        }
    });

    $('.recommend-change-page').change(function () {
        page = $(this).val();
        url = '/?page=' + page;
        window.location.href = url;
    });

    function windowResize() {
        $('.thumb > a > img').each(function () {
            var tw = $(this).parents('.item').width();
            var th = tw * 0.6;
            $(this).parents('.thumb').width(tw).height(th);
            $(this).width(tw);
        });
    }

    function lazyRender() {
        $('.thumb > a > img').each(function () {
            if (checkShow($(this)) && !isLoaded($(this))) {
                loadImg($(this));
            }
        });
    }

    function checkShow($img) {
        // 即页面向上滚动的距离
        var scrollTop = $(window).scrollTop();
        // 浏览器自身的高度
        var windowHeight = $(window).height();
        // 目标标签img相对于document顶部的位置
        var offsetTop = $img.offset().top;
        // 在2个临界状态之间的就为出现在视野中的
        if (offsetTop < (scrollTop + windowHeight) && offsetTop > scrollTop) {
            return true;
        }
        return false;
    }

    function isLoaded($img) {
        // 如果data-src和src相等那么就是已经加载过了
        return $img.attr('data-src') === $img.attr('src');
    }

    function loadImg($img) {
        // 加载就是把自定义属性中存放的真实的src地址赋给src属性
        $img.attr('src', $img.attr('data-src'));
    }

});